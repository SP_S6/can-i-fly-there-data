const fs = require('fs');

fs.readFile('./caniflythere-msfs.csv', (err, data) => {
    if (err) {
        throw err; 
    }

    const csvData = data.toString();
    const lines = csvData.split("\n");

    let entries = {};

    for (let i = 1; i < lines.length; i++) {
        const line = lines[i].split(",");

        // Get the data out of the line
        const fseIcao = line[0];
        const msfsIcao = line[11];

        if (msfsIcao == "") {
            entries[fseIcao] = { status: "missing" };
        } else if (fseIcao == msfsIcao) {
            entries[fseIcao] = { status: "exists" };
        } else {
            entries[fseIcao] = { status: "renamed", simIcao: msfsIcao };
        }
    }

    console.log(entries);

    fs.writeFile(
        "./caniflythere-msfs.json",
        JSON.stringify(entries),
        () => console.log("Wrote to file")
    );
});